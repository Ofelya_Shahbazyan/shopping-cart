CREATE DATABASE IF NOT EXISTS shoppingcart;

USE shoppingcart;

--
-- ROLE
--

CREATE TABLE IF NOT EXISTS role(
                                   role_id  bigint(20) NOT NULL AUTO_INCREMENT,
                                   role_name   varchar(255) DEFAULT NULL,
                                   PRIMARY KEY (role_id)
)ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


LOCK TABLES role WRITE;
/*!40000 ALTER TABLE role DISABLE KEYS */;
REPLACE INTO role
VALUES (1, 'ADMIN'),
       (2, 'USER');
/*!40000 ALTER TABLE role ENABLE KEYS */;
UNLOCK TABLES;


--
-- USER
--

CREATE TABLE  IF NOT EXISTS user(
                                    user_id bigint(20) NOT NULL AUTO_INCREMENT,
                                    email varchar(255) NOT NULL,
                                    is_account_non_expired bit(1) DEFAULT NULL,
                                    is_account_non_locked bit(1) DEFAULT NULL,
                                    is_credentials_non_expired bit(1) DEFAULT NULL,
                                    is_enabled bit(1) DEFAULT NULL,
                                    name varchar(255) NOT NULL,
                                    password varchar(255) NOT NULL,
                                    surname varchar(255) NOT NULL,
                                    username varchar(255) NOT NULL,
                                    PRIMARY KEY (user_id)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

LOCK TABLES shoppingcart.user WRITE;
/*!40000 ALTER TABLE shoppingcart.user DISABLE KEYS */;
REPLACE INTO shoppingcart.user
VALUES ('1', 'shahbazyanofelya.9@gmail.com',true, true, true, true, 'Ofelya', '$2a$10$FTRgcZipE3AH1JujhZlHAu5jmHyagoFMW89rb8kW2U/k18JOk7L7i', 'Shahbazyan',
         'admin');
/*!40000 ALTER TABLE shoppingcart.user ENABLE KEYS */;
UNLOCK TABLES;


--
-- USER_ROLE
--
CREATE TABLE IF NOT EXISTS  user_role (
                                          user_id bigint(20) NOT NULL,
                                          role_id bigint(20) NOT NULL,
                                          PRIMARY KEY (user_id,role_id),
                                          KEY role_id_fk  (role_id),
                                          KEY user_id_fk (user_id),
                                          CONSTRAINT role_id_fk
                                              FOREIGN KEY (role_id)
                                                  REFERENCES role (role_id)
                                              ON DELETE CASCADE
                                              ON UPDATE CASCADE,
                                          CONSTRAINT user_id_fk
                                              FOREIGN KEY (user_id)
                                                  REFERENCES user (user_id)
                                              ON DELETE CASCADE
                                              ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

LOCK TABLES shoppingcart.user_role WRITE;
/*!40000 ALTER TABLE shoppingcart.user_role DISABLE KEYS */;
REPLACE INTO shoppingcart.user_role VALUES (1,1);
/*!40000 ALTER TABLE shoppingcart.user_role ENABLE KEYS */;
UNLOCK TABLES;
# DELETE FROM shoppingcart.user_role
# WHERE user_role.user_id = '1';
# INSERT INTO  shoppingcart.user_role
# VALUES (1, 1);
#



--
-- Shopping_Carts
--

CREATE TABLE IF NOT EXISTS shopping_carts (
                                  cart_id bigint(20) NOT NULL AUTO_INCREMENT,
                                  user_id bigint(20) DEFAULT NULL,
                                  PRIMARY KEY (cart_id, user_id),
                                  KEY user_id_fk (user_id),
                                      CONSTRAINT user_id_fk
                                          FOREIGN KEY (user_id)
                                              REFERENCES user (user_id)
                                              ON DELETE CASCADE
                                              ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


--
-- Product
--

CREATE TABLE IF NOT EXISTS products (
                            product_id bigint(20) NOT NULL AUTO_INCREMENT,
                            added_date datetime NOT NULL,
                            count_in_stock int(11) NOT NULL,
                            name varchar(255) NOT NULL,
                            price decimal(19,2) NOT NULL,
                            updated_date datetime NOT NULL,
                            product_type_type_id bigint(20) DEFAULT NULL,
                            PRIMARY KEY (product_id),
                            KEY product_type_id_fk  (product_type_type_id),
                            CONSTRAINT product_type_id_fk
                                FOREIGN KEY (product_type_type_id)
                                    REFERENCES product_types (type_id)
                                    ON DELETE CASCADE
                                    ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;



--
-- Product Types
--


LOCK TABLES product_types WRITE;
/*!40000 ALTER TABLE product_types DISABLE KEYS */;
REPLACE INTO product_types
VALUES (1, 'Book'),
       (2, 'Food'),
       (3, 'Drink'),
       (4, 'Clothe'),
       (5, 'Toy');
/*!40000 ALTER TABLE product_types ENABLE KEYS */;
UNLOCK TABLES;



--
-- Orders
--

CREATE TABLE IF NOT EXISTS orders (
                          order_id bigint(20) NOT NULL AUTO_INCREMENT,
                          buyer_email varchar(255) DEFAULT NULL,
                          buyer_name varchar(255) DEFAULT NULL,
                          buyer_surname varchar(255) DEFAULT NULL,
                          count_in_stock int(11) DEFAULT NULL,
                          created_time datetime DEFAULT NULL,
                          order_status varchar(255) DEFAULT NULL,
                          product_name varchar(255) DEFAULT NULL,
                          product_price decimal(19,2) DEFAULT NULL,
                          product_type varchar(255) DEFAULT NULL,
                          total_amount decimal(19,2) DEFAULT NULL,
                          total_count int(11) DEFAULT NULL,
                          updated_time datetime DEFAULT NULL,
                          cart_id bigint(20) DEFAULT NULL,
                          PRIMARY KEY (order_id),
                          KEY cart_id_fk (cart_id),
                          CONSTRAINT cart_id_fk
                              FOREIGN KEY (cart_id)
                                  REFERENCES shopping_carts (cart_id)
                                  ON DELETE CASCADE
                                  ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Notifications
--


CREATE TABLE IF NOT EXISTS notifications (
                                 id bigint(20) NOT NULL AUTO_INCREMENT,
                                 created_date datetime DEFAULT NULL,
                                 message varchar(255) DEFAULT NULL,
                                 admin_id bigint(20) NOT NULL,
                                 user_id bigint(20) NOT NULL,
                                 PRIMARY KEY (id),
                                 KEY admin_id_fk (admin_id),
                                 KEY user_id_fkey (user_id),
                                 CONSTRAINT admin_id_fk
                                     FOREIGN KEY (admin_id)
                                         REFERENCES user (user_id),
                                 CONSTRAINT user_id_fkey
                                     FOREIGN KEY (user_id)
                                         REFERENCES user (user_id)
                                         ON DELETE CASCADE
                                         ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;










