package am.shopping.cart.bean.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "product_types")
public class ProductType implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "type_id", columnDefinition = "BIGINT(20)", nullable = false)
    private Long id;

    @Column(name = "type_name")
    private String type;

    public ProductType() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
