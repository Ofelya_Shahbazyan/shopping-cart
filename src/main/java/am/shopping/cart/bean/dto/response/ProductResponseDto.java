package am.shopping.cart.bean.dto.response;

import am.shopping.cart.bean.entity.ProductType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ProductResponseDto implements Serializable {

    private Long id;
    private String name;
    private BigDecimal price;
    private Date addedDate;
    private Date updatedDate;
    private Integer countInStock;
    private ProductType productType;

    public ProductResponseDto() {
    }

    public ProductResponseDto(Long id, String name, BigDecimal price, Date addedDate, Date updatedDate, Integer countInStock, ProductType productType) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.addedDate = addedDate;
        this.updatedDate = updatedDate;
        this.countInStock = countInStock;
        this.productType = productType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Integer getCountInStock() {
        return countInStock;
    }

    public void setCountInStock(Integer countInStock) {
        this.countInStock = countInStock;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }
}
