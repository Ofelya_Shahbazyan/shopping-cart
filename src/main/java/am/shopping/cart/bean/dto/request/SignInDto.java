package am.shopping.cart.bean.dto.request;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;

public class SignInDto implements Serializable {

    @NotEmpty(message = "{username.notEmpty}")
    @Size(min = 5, max = 12, message = "{username.size}")
    private String username;

    @NotEmpty(message = "{password.notEmpty}")
    private String password;

    public SignInDto() {
    }

    public SignInDto(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
