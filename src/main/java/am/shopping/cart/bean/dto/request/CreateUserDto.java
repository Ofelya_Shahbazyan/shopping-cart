package am.shopping.cart.bean.dto.request;

import am.shopping.cart.validation.EmailValidation;
import am.shopping.cart.validation.IsUniqueValidation;
import am.shopping.cart.validation.PasswordValidation;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;

public class CreateUserDto implements Serializable {

    @NotEmpty(message = "{name.notEmpty}")
    @Size(min = 2, max = 12, message = "{name.size}")
    private String name;

    @NotEmpty(message = "{surname.notEmpty}")
    @Size(min = 2, max = 12, message = "{surname.size}")
    private String surname;

    @NotEmpty(message = "{email.notEmpty}")
    @EmailValidation
    private String email;

    @NotEmpty(message = "{username.notEmpty}")
    @Size(min = 5, max = 12, message = "{username.size}")
    @IsUniqueValidation(fieldName = "Username")
    private String username;

    @NotEmpty(message = "{password.notEmpty}")
    @PasswordValidation
    private String password;

    public CreateUserDto() {
    }

    public CreateUserDto(String name, String surname, String email, String username, String password) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.username = username;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
