package am.shopping.cart.bean.dto.request;

import am.shopping.cart.bean.entity.ProductType;
import am.shopping.cart.validation.PriceValidation;
import am.shopping.cart.validation.ProductTypeValidation;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;

public class AddProductDto implements Serializable {

    private Long id;

    @NotEmpty(message = "{name.notEmpty}")
    @Size(min = 3, max = 50, message = "{product.name.size}")
    private String name;

    //@DecimalMin(value = "1.0", inclusive = false, message = "{price.min}")
    @PriceValidation
    private BigDecimal price;

    @ProductTypeValidation
    private ProductType productType;

    public AddProductDto() {
    }

    public AddProductDto(Long id, String name, BigDecimal price, ProductType productType) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.productType = productType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }
}
