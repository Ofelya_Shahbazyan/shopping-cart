package am.shopping.cart.bean.dto.response;

import java.io.Serializable;

public class AuthenticationResponseDto implements Serializable {

    private final String jwt;

    public AuthenticationResponseDto(String jwt) {
        this.jwt = jwt;
    }

    public String getJwt() {
        return jwt;
    }
}
