package am.shopping.cart.bean.dto.response;

import am.shopping.cart.bean.entity.ProductType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class OrderResponseDto implements Serializable {

    private String productName;
    private String productType;
    private BigDecimal productPrice;
    private Integer totalCount;
    private BigDecimal totalAmount;
    private String orderStatus;
    private Date createdTime;
    private Date updatedTime;

    public OrderResponseDto() {
    }

    public OrderResponseDto(String productName, String productType, BigDecimal productPrice, Integer totalCount,
                            BigDecimal totalAmount, String orderStatus, Date createdTime, Date updatedTime) {
        this.productName = productName;
        this.productType = productType;
        this.productPrice = productPrice;
        this.totalCount = totalCount;
        this.totalAmount = totalAmount;
        this.orderStatus = orderStatus;
        this.createdTime = createdTime;
        this.updatedTime = updatedTime;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public BigDecimal getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(BigDecimal productPrice) {
        this.productPrice = productPrice;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }
}
