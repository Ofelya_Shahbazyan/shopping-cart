package am.shopping.cart.bean.dto.request;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

public class CreateOrderDto implements Serializable {

    @NotNull(message = "{total.count.notEmpty}")
    @Min(value = 1, message = "{total.count.min}")
    private Integer totalCount;

    public CreateOrderDto() {
    }

    public CreateOrderDto(Long productId, Integer totalCount) {
//        this.productId = productId;
        this.totalCount = totalCount;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }
}
