package am.shopping.cart.controller;

import am.shopping.cart.bean.Pager;
import am.shopping.cart.bean.entity.Order;
import am.shopping.cart.exception.OperationFailedException;
import am.shopping.cart.service.OrderResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Optional;

@RestController
@Secured("ROLE_ADMIN")
@RequestMapping("/order/resource")
public class OrderResourceController {

    private static final int BUTTONS_TO_SHOW = 5;
    private static final int INITIAL_PAGE = 0;
    private static final int INITIAL_PAGE_SIZE = 5;
    private static final int[] PAGE_SIZES = {5, 10, 20};

    private OrderResourceService orderResourceService;

    @Autowired
    public OrderResourceController(OrderResourceService orderResourceService) {
        this.orderResourceService = orderResourceService;
    }

    @GetMapping("/all")
    public ResponseEntity showAllOrders(@RequestParam("pageSize") Optional<Integer> pageSize,
                                        @RequestParam("page") Optional<Integer> page) {
        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
        Page<Order> orders = orderResourceService.findAllOrders(PageRequest.of(evalPage, evalPageSize));
        if (orders.isEmpty()) {
            return ResponseEntity.ok().body("There are no orders.");
        }
        Pager pager = new Pager(orders.getTotalPages(), orders.getNumber(), BUTTONS_TO_SHOW);
        HashMap<String, Object> hmap = new HashMap<>();
        hmap.put("orders", orders);
        hmap.put("pager", pager);
        hmap.put("selectedPageSize", evalPageSize);
        hmap.put("pageSizes", PAGE_SIZES);
        return new ResponseEntity<>(hmap, HttpStatus.OK);
    }

    @PutMapping("/accept/{id}")
    public ResponseEntity accept(@PathVariable("id") Long id) {
        String statusInDb = orderResourceService.findById(id).getOrderStatus();
        if (!statusInDb.equals("New")) {
            throw new OperationFailedException("This order already " + statusInDb);
        } else {
            orderResourceService.accept(id);
        }
        return ResponseEntity.ok().body("Order " + orderResourceService.findById(id).getOrderStatus());
    }

    @PutMapping("/reject/{id}")
    public ResponseEntity reject(@PathVariable("id") Long id) {
        String statusInDb = orderResourceService.findById(id).getOrderStatus();
        if (!statusInDb.equals("New")) {
            throw new OperationFailedException("This order already " + statusInDb);
        } else {
            orderResourceService.reject(id);
        }
        return ResponseEntity.ok().body("Order " + orderResourceService.findById(id).getOrderStatus());
    }

}
