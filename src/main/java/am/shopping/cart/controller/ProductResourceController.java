package am.shopping.cart.controller;

import am.shopping.cart.bean.Pager;
import am.shopping.cart.bean.entity.Product;
import am.shopping.cart.service.ProductResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Optional;

@RestController
@RequestMapping(value = "/product/resource")
@Secured({"ROLE_ADMIN", "ROLE_USER"})
public class ProductResourceController {

    private static final int BUTTONS_TO_SHOW = 5;
    private static final int INITIAL_PAGE = 0;
    private static final int INITIAL_PAGE_SIZE = 5;
    private static final int[] PAGE_SIZES = {5, 10, 20};

    private ProductResourceService productResourceService;

    @Autowired
    public ProductResourceController(ProductResourceService productResourceService) {
        this.productResourceService = productResourceService;
    }

    @GetMapping("/all")
    public ResponseEntity getAllProduct(@RequestParam("pageSize") Optional<Integer> pageSize,
                                        @RequestParam("page") Optional<Integer> page) {
        // Evaluate page size. If requested parameter is null, return initial
        // page size
        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        // Evaluate page. If requested parameter is null or less than 0 (to
        // prevent exception), return initial size. Otherwise, return value of
        // param. decreased by 1.
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
        Page<Product> productList = productResourceService.findAll(PageRequest.of(evalPage, evalPageSize));
        Pager pager = new Pager(productList.getTotalPages(), productList.getNumber(), BUTTONS_TO_SHOW);
        HashMap<String, Object> hmap = new HashMap<>();
        hmap.put("products", productList);
        hmap.put("pager", pager);
        hmap.put("selectedPageSize", evalPageSize);
        hmap.put("pageSizes", PAGE_SIZES);
        return new ResponseEntity<>(hmap, HttpStatus.OK);
    }

    @GetMapping("/search/by/name")
    public ResponseEntity searchByName(@RequestParam String name) {
        Product product = productResourceService.findByNameLike(name);
        if (product == null) {
            return ResponseEntity.ok().body("There are no product with this name.");
        }
        return ResponseEntity.ok().body(product);
    }

    @GetMapping("/search/by/type")
    public ResponseEntity searchByType(@RequestParam String type, @RequestParam("pageSize") Optional<Integer> pageSize,
                                       @RequestParam("page") Optional<Integer> page) {
        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
        Page<Product> productList = productResourceService.findByProductType_TypeLike(type, PageRequest.of(evalPage, evalPageSize));
        if (productList.isEmpty()) {
            return ResponseEntity.ok().body("There are no product like this type.");
        }
        Pager pager = new Pager(productList.getTotalPages(), productList.getNumber(), BUTTONS_TO_SHOW);
        HashMap<String, Object> hmap = new HashMap<>();
        hmap.put("products", productList);
        hmap.put("pager", pager);
        hmap.put("selectedPageSize", evalPageSize);
        hmap.put("pageSizes", PAGE_SIZES);
        return new ResponseEntity<>(hmap, HttpStatus.OK);
    }

    @GetMapping("/search/by/price/ascending")
    public ResponseEntity searchByPrice(@RequestParam("pageSize") Optional<Integer> pageSize,
                                        @RequestParam("page") Optional<Integer> page) {
        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
        Page<Product> productList = productResourceService.findAllByOrderByPriceAsc(PageRequest.of(evalPage, evalPageSize));
        if (productList.isEmpty()) {
            return ResponseEntity.ok().body("There are no product.");
        }
        Pager pager = new Pager(productList.getTotalPages(), productList.getNumber(), BUTTONS_TO_SHOW);
        HashMap<String, Object> hmap = new HashMap<>();
        hmap.put("products", productList);
        hmap.put("pager", pager);
        hmap.put("selectedPageSize", evalPageSize);
        hmap.put("pageSizes", PAGE_SIZES);
        return new ResponseEntity<>(hmap, HttpStatus.OK);
    }
}
