package am.shopping.cart.controller;

import am.shopping.cart.bean.dto.response.ErrorResponseDto;
import am.shopping.cart.exception.OperationFailedException;
import javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@SuppressWarnings({"unchecked", "rawtypes"})
@ControllerAdvice
class ErrorController {


    @ExceptionHandler(Throwable.class)
    protected ResponseEntity customExceptionHandler(Exception ex) throws Exception {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());

        if (ex instanceof MethodArgumentNotValidException) {
            List<String> exc = new ArrayList<>();

            for (ObjectError error : ((MethodArgumentNotValidException) ex).getBindingResult().getAllErrors()) {
                exc.add(error.getDefaultMessage());
            }
            ErrorResponseDto error = new ErrorResponseDto("Validation failed!", exc);
            return new ResponseEntity(error, HttpStatus.BAD_REQUEST);
        } else if (ex instanceof BadCredentialsException) {
            ErrorResponseDto error = new ErrorResponseDto("Incorrect username or password!", details);
            return new ResponseEntity(error, HttpStatus.UNAUTHORIZED);
        } else if (ex instanceof AccessDeniedException) {
            ErrorResponseDto error = new ErrorResponseDto("Access denied!", details);
            return new ResponseEntity(error, HttpStatus.FORBIDDEN);
        } else if (ex instanceof NullPointerException) {
            ErrorResponseDto error = new ErrorResponseDto("Null!", details);
            return new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
        } else if (ex instanceof NotFoundException) {
            ErrorResponseDto error = new ErrorResponseDto("There is no product with this name!", details);
            return new ResponseEntity(error, HttpStatus.NOT_FOUND);
        } else if (ex instanceof OperationFailedException) {
            ErrorResponseDto error = new ErrorResponseDto("Operation failed!", details);
            return new ResponseEntity(error, HttpStatus.BAD_REQUEST);
        } else if (ex instanceof NoSuchElementException) {
            ErrorResponseDto error = new ErrorResponseDto("There is no order with this id", details);
            return new ResponseEntity(error, HttpStatus.BAD_REQUEST);
        } else if (ex instanceof MissingServletRequestParameterException) {
            ErrorResponseDto error = new ErrorResponseDto("Operation failed!", details);
            return new ResponseEntity(error, HttpStatus.BAD_REQUEST);
        } else {
            ErrorResponseDto error = new ErrorResponseDto(ex.getMessage(), details);
            return new ResponseEntity(error, HttpStatus.BAD_REQUEST);
        }

    }
}



