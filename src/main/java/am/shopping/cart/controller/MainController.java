package am.shopping.cart.controller;

import am.shopping.cart.bean.dto.request.CreateUserDto;
import am.shopping.cart.bean.dto.request.SignInDto;
import am.shopping.cart.bean.dto.response.AuthenticationResponseDto;
import am.shopping.cart.bean.dto.response.SignUpResponseDto;
import am.shopping.cart.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class MainController {

    private UserService userService;

    @Autowired
    public MainController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/register")
    public ResponseEntity registrationNewUser(@Valid @RequestBody CreateUserDto createUserDto) {
        SignUpResponseDto signUpResponseDto = userService.addUser(createUserDto);
        return ResponseEntity.ok().body(signUpResponseDto);
    }

    @PostMapping("/login")
    public ResponseEntity authenticateUser(@Valid @RequestBody SignInDto signInDto) {
        AuthenticationResponseDto authenticationResponseDto = userService.signIn(signInDto);
        return ResponseEntity.ok().body(authenticationResponseDto);
    }
}
