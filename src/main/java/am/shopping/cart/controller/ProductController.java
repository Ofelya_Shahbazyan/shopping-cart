package am.shopping.cart.controller;

import am.shopping.cart.bean.dto.request.AddProductDto;
import am.shopping.cart.bean.dto.request.EditProductDto;
import am.shopping.cart.bean.dto.response.ProductResponseDto;
import am.shopping.cart.bean.entity.Product;
import am.shopping.cart.service.ProductService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/product")
@Secured("ROLE_ADMIN")
public class ProductController {

    private ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping("/add")
    public ResponseEntity<ProductResponseDto> addProduct(@Valid @RequestBody AddProductDto addProductDto) {
        ProductResponseDto productResponseDto = productService.addProduct(addProductDto);
        return ResponseEntity.ok().body(productResponseDto);
    }

    @GetMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable("id") Long id) throws NotFoundException {
        productService.deleteById(id);
        return ResponseEntity.ok().body("Product deleted.");
    }

    @PutMapping("/edit/{id}")
    public ResponseEntity editProduct(@PathVariable("id") Long id, @Valid @RequestBody EditProductDto editProductDto)
            throws NotFoundException {
        Product product = productService.editProduct(id, editProductDto);
        return ResponseEntity.ok().body(product);
    }

}
