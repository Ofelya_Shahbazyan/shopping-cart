package am.shopping.cart.controller;

import am.shopping.cart.bean.Pager;
import am.shopping.cart.bean.entity.Notification;
import am.shopping.cart.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Optional;

@RestController
@RequestMapping("/notification")
@Secured("ROLE_USER")
public class NotificationController {

    private static final int BUTTONS_TO_SHOW = 5;
    private static final int INITIAL_PAGE = 0;
    private static final int INITIAL_PAGE_SIZE = 5;
    private static final int[] PAGE_SIZES = {5, 10, 20};

    private NotificationService notificationService;

    @Autowired
    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @GetMapping("/get/all")
    public ResponseEntity getNotification(@RequestParam("pageSize") Optional<Integer> pageSize,
                                          @RequestParam("page") Optional<Integer> page) {
        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

        Page<Notification> notificationList = notificationService.getNotificationByRecipient(PageRequest.of(evalPage, evalPageSize));
        if (notificationList.isEmpty()) {
            return ResponseEntity.ok().body("There are no notifications.");
        }
        Pager pager = new Pager(notificationList.getTotalPages(), notificationList.getNumber(), BUTTONS_TO_SHOW);
        HashMap<String, Object> hmap = new HashMap<>();
        hmap.put("notifications", notificationList);
        hmap.put("pager", pager);
        hmap.put("selectedPageSize", evalPageSize);
        hmap.put("pageSizes", PAGE_SIZES);
        return new ResponseEntity<>(hmap, HttpStatus.OK);
    }

}
