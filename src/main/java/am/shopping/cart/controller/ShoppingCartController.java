package am.shopping.cart.controller;

import am.shopping.cart.bean.Pager;
import am.shopping.cart.bean.entity.Order;
import am.shopping.cart.bean.entity.User;
import am.shopping.cart.service.OrderService;
import am.shopping.cart.util.CurrentUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Optional;

@RestController
@Secured("ROLE_USER")
@RequestMapping("/shopping/cart")
public class ShoppingCartController {
    private static final int BUTTONS_TO_SHOW = 5;
    private static final int INITIAL_PAGE = 0;
    private static final int INITIAL_PAGE_SIZE = 5;
    private static final int[] PAGE_SIZES = {5, 10, 20};

    private CurrentUser currentUser;
    private OrderService orderService;

    @Autowired
    public ShoppingCartController(CurrentUser currentUser, OrderService orderService) {
        this.currentUser = currentUser;
        this.orderService = orderService;
    }

    @GetMapping("/details")
    public ResponseEntity showDetails(@RequestParam("pageSize") Optional<Integer> pageSize,
                                      @RequestParam("page") Optional<Integer> page) {
        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
        User user = currentUser.getCurrentUser();
        Page<Order> orderList = orderService.findAllOrders(user, PageRequest.of(evalPage, evalPageSize));
        if (orderList.isEmpty()) {

            return ResponseEntity.ok().body("There are no orders.");
        }
        Pager pager = new Pager(orderList.getTotalPages(), orderList.getNumber(), BUTTONS_TO_SHOW);
        HashMap<String, Object> hmap = new HashMap<>();
        hmap.put("orders", orderList);
        hmap.put("pager", pager);
        hmap.put("selectedPageSize", evalPageSize);
        hmap.put("pageSizes", PAGE_SIZES);
        return new ResponseEntity<>(hmap, HttpStatus.OK);
    }

}
