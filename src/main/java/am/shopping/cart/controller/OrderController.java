package am.shopping.cart.controller;

import am.shopping.cart.bean.dto.request.CreateOrderDto;
import am.shopping.cart.bean.dto.response.OrderResponseDto;
import am.shopping.cart.service.OrderService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/order")
@Secured("ROLE_USER")
public class OrderController {

    private OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping("/new/{id}")
    public ResponseEntity newOrder(@PathVariable("id") Long id, @RequestBody CreateOrderDto createOrderDto) throws NotFoundException {
        OrderResponseDto orderResponseDto = orderService.newOrder(id, createOrderDto);
        return ResponseEntity.ok().body(orderResponseDto);
    }

}
