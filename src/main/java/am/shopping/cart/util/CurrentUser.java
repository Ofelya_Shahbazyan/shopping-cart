package am.shopping.cart.util;

import am.shopping.cart.bean.entity.User;
import am.shopping.cart.security.CustomUserDetails;
import am.shopping.cart.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class CurrentUser {

    private UserService userService;

    @Autowired
    public CurrentUser(UserService userService) {
        this.userService = userService;
    }

    public User getCurrentUser() {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = customUserDetails.getUsername();
        Optional<User> user = userService.findUserByUsername(username);
        return user.get();
    }
}

