package am.shopping.cart.mapper;

import am.shopping.cart.bean.dto.request.SignInDto;
import am.shopping.cart.bean.entity.User;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingException;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class UserMapper extends ConfigurableMapper {

    @Override
    protected void configure(MapperFactory factory) {
        String fieldName;
        try {
            fieldName = SignInDto.class.getDeclaredField("password").getName();
        } catch (NoSuchFieldException ignored) {
            throw new MappingException("Ignored field dose not exist.");
        }
        factory.classMap(SignInDto.class, User.class).fieldAToB(fieldName, fieldName).byDefault().register();
    }

}
