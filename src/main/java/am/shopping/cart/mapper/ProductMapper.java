package am.shopping.cart.mapper;

import am.shopping.cart.bean.dto.request.AddProductDto;
import am.shopping.cart.bean.entity.Product;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingException;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class ProductMapper extends ConfigurableMapper {

    @Override
    protected void configure(MapperFactory factory) {
        String fieldName;
        try {
            fieldName = AddProductDto.class.getDeclaredField("name").getName();
        } catch (NoSuchFieldException ignored) {
            throw new MappingException("Ignored field dose not exist.");
        }
        factory.classMap(AddProductDto.class, Product.class).fieldAToB(fieldName, fieldName).byDefault().register();
    }

}
