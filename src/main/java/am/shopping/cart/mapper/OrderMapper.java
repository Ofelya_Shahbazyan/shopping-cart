package am.shopping.cart.mapper;

import am.shopping.cart.bean.dto.request.CreateOrderDto;
import am.shopping.cart.bean.entity.Order;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingException;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class OrderMapper extends ConfigurableMapper {

    @Override
    protected void configure(MapperFactory factory) {
        String fieldName;
        try {
            fieldName = CreateOrderDto.class.getDeclaredField("totalCount").getName();
        } catch (NoSuchFieldException ignored) {
            throw new MappingException("Ignored field dose not exist.");
        }
        factory.classMap(CreateOrderDto.class, Order.class).fieldAToB(fieldName, fieldName).byDefault().register();
    }

}
