package am.shopping.cart.mapper;

import am.shopping.cart.bean.dto.response.NotificationDto;
import am.shopping.cart.bean.entity.Notification;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class NotificationMapper extends ConfigurableMapper {
    @Override
    protected void configure(MapperFactory factory) {

        factory.classMap(Notification.class, NotificationDto.class)
                .byDefault()
                .register();
    }
}
