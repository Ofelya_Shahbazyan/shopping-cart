package am.shopping.cart.repository;

import am.shopping.cart.bean.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface ProductResourceRepository extends JpaRepository<Product, Long> {

    @Query(value = "SELECT * FROM products p",
            countQuery = "SELECT count(*) FROM products",
            nativeQuery = true)
    Page<Product> findAllProducts(Pageable pageable);

    @Query(value = "SELECT p FROM Product p WHERE p.name = :name")
    Product getProductByName(@Param("name") String name);

    @Query(value = "SELECT p FROM Product p JOIN p.productType pt WHERE pt.type = :productType",
            countQuery = "SELECT count(p) FROM Product p JOIN p.productType pt WHERE pt.type = :productType")
    Page<Product> findByProductType_TypeLike(@Param("productType") String type, Pageable pageable);

    @Query(value = "SELECT * FROM products p ORDER BY p.price ASC",
            countQuery = "SELECT count(*) FROM products p ORDER BY p.price ASC",
            nativeQuery = true)
    Page<Product> findAllByOrderByPriceAsc(Pageable pageable);
}
