package am.shopping.cart.repository;

import am.shopping.cart.bean.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
@Transactional
public interface RoleRepository extends JpaRepository<Role, Long> {

    @Query("SELECT r from Role r where r.role = :role")
    Optional<Role> findByRole(@Param("role") String role);
}
