package am.shopping.cart.repository;

import am.shopping.cart.bean.entity.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
@Transactional
public interface OrderRepository extends JpaRepository<Order, Long> {

    @Query(value = "SELECT o FROM Order o JOIN o.shoppingCart shc WHERE shc.id = :id",
            countQuery = "SELECT count(o) FROM Order o JOIN o.shoppingCart shc WHERE shc.id = :id")
    Page<Order> findAllByShoppingCart(@Param("id") Long id, Pageable pageable);
}
