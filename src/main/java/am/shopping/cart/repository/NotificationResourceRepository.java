package am.shopping.cart.repository;

import am.shopping.cart.bean.entity.Notification;
import am.shopping.cart.bean.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface NotificationResourceRepository extends JpaRepository<Notification, Long> {

    @Query(value = "Select * from notifications u where u.admin_id = :id ORDER BY u.created_date",
            countQuery = "SELECT count(*) from notifications u where u.admin_id = :id ORDER BY u.created_date",
            nativeQuery = true)
    Page<Notification> getAll(@Param("id") User adminId, Pageable pageable);

}
