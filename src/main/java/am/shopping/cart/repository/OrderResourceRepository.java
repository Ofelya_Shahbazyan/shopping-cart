package am.shopping.cart.repository;

import am.shopping.cart.bean.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface OrderResourceRepository extends JpaRepository<Order, Long> {

}
