package am.shopping.cart.repository;

import am.shopping.cart.bean.entity.ProductType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
@Transactional
public interface ProductTypeRepository extends JpaRepository<ProductType, Long> {

    @Query("select pt FROM ProductType pt where pt.id = :id and pt.type = :type")
    Optional<ProductType> findByTypeAndId(@Param("type") String type, @Param("id") Long id);
}
