package am.shopping.cart.service.impl;

import am.shopping.cart.bean.entity.ProductType;
import am.shopping.cart.repository.ProductTypeRepository;
import am.shopping.cart.service.ProductTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class ProductTypeServiceImpl implements ProductTypeService {

    private ProductTypeRepository productTypeRepository;

    @Autowired
    public ProductTypeServiceImpl(ProductTypeRepository productTypeRepository) {
        this.productTypeRepository = productTypeRepository;
    }

    @Override
    public Optional<ProductType> findByTypeAndId(String type, Long id) {
        return productTypeRepository.findByTypeAndId(type, id);
    }
}
