package am.shopping.cart.service.impl;

import am.shopping.cart.bean.entity.Notification;
import am.shopping.cart.bean.entity.User;
import am.shopping.cart.mapper.NotificationMapper;
import am.shopping.cart.repository.NotificationResourceRepository;
import am.shopping.cart.service.NotificationResourceService;
import am.shopping.cart.util.CurrentUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class NotificationResourceServiceImpl implements NotificationResourceService {

    private NotificationResourceRepository notificationResourceRepository;
    private NotificationMapper notificationMapper;
    private CurrentUser currentUser;

    @Autowired
    public NotificationResourceServiceImpl(NotificationResourceRepository notificationResourceRepository, NotificationMapper notificationMapper, CurrentUser currentUser) {
        this.notificationResourceRepository = notificationResourceRepository;
        this.notificationMapper = notificationMapper;
        this.currentUser = currentUser;
    }

    @Override
    public Page<Notification> getAllNotifications(Pageable pageable) {
        User admin = currentUser.getCurrentUser();
        Page<Notification> notifications = notificationResourceRepository.getAll(admin, pageable);
//        return notificationMapper.mapAsList(notifications, NotificationDto.class);
        return notifications;
    }
}
