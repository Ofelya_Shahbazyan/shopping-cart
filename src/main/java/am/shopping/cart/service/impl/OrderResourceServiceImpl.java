package am.shopping.cart.service.impl;

import am.shopping.cart.bean.entity.Notification;
import am.shopping.cart.bean.entity.Order;
import am.shopping.cart.bean.entity.Product;
import am.shopping.cart.repository.OrderResourceRepository;
import am.shopping.cart.repository.ProductRepository;
import am.shopping.cart.repository.UserRepository;
import am.shopping.cart.service.NotificationService;
import am.shopping.cart.service.OrderResourceService;
import am.shopping.cart.util.CurrentUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Optional;

@Service
@Transactional
public class OrderResourceServiceImpl implements OrderResourceService {

    private OrderResourceRepository orderResourceRepository;
    private ProductRepository productRepository;
    private UserRepository userRepository;
    private CurrentUser currentUser;
    private NotificationService notificationService;

    @Autowired
    public OrderResourceServiceImpl(OrderResourceRepository orderResourceRepository, ProductRepository productRepository, UserRepository userRepository, CurrentUser currentUser, NotificationService notificationService) {
        this.orderResourceRepository = orderResourceRepository;
        this.productRepository = productRepository;
        this.userRepository = userRepository;
        this.currentUser = currentUser;
        this.notificationService = notificationService;
    }

    @Override
    public Page<Order> findAllOrders(Pageable pageable) {
        return orderResourceRepository.findAll(pageable);
    }

    @Override
    public Order findById(Long id) {
        return orderResourceRepository.findById(id).get();
    }

    @Override
    public void accept(Long id) {
        Order order = orderResourceRepository.findById(id).get();
        Optional<Product> productInDb = productRepository.findProductByName(order.getProductName());
        if (order.getTotalCount() <= productInDb.get().getCountInStock()) {
            order.setOrderStatus("Accepted");
            order.setUpdatedTime(new Date());
            orderResourceRepository.save(order);
            productInDb.get().setCountInStock(productInDb.get().getCountInStock() - order.getTotalCount());
            productInDb.get().setUpdatedDate(new Date());
            productRepository.save(productInDb.get());
            Notification notification = Notification.builder()
                    .admin(userRepository.findById((long) 1).get())
                    .user(order.getShoppingCart().getUser())
                    .message("Well done! The order successfully accepted and has  'Accepted' status.")
                    .build();

            notificationService.addNotification(notification);
        } else {
            reject(id);
        }
    }

    public void reject(Long id) {
        Order order = orderResourceRepository.findById(id).get();
        order.setOrderStatus("Rejected");
        order.setUpdatedTime(new Date());
        orderResourceRepository.save(order);
        Notification notification = Notification.builder()
                .admin(userRepository.findById((long) 1).get())
                .user(order.getShoppingCart().getUser())
                .message("Well done! The order successfully rejected and has  'Rejected' status.")
                .build();
        notificationService.addNotification(notification);
    }
}
