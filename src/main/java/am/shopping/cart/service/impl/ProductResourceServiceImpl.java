package am.shopping.cart.service.impl;

import am.shopping.cart.bean.entity.Product;
import am.shopping.cart.repository.ProductResourceRepository;
import am.shopping.cart.service.ProductResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ProductResourceServiceImpl implements ProductResourceService {

    private ProductResourceRepository productResourceRepository;

    @Autowired
    public ProductResourceServiceImpl(ProductResourceRepository productResourceRepository) {
        this.productResourceRepository = productResourceRepository;
    }

    @Override
    public Page<Product> findAll(Pageable pageable) {
        return productResourceRepository.findAllProducts(pageable);
    }

    @Override
    public Product findByNameLike(String name) {
        return productResourceRepository.getProductByName(name);
    }

    @Override
    public Page<Product> findByProductType_TypeLike(String type, Pageable pageable) {
        return productResourceRepository.findByProductType_TypeLike(type, pageable);
    }

    @Override
    public Page<Product> findAllByOrderByPriceAsc(Pageable pageable) {
        return productResourceRepository.findAllByOrderByPriceAsc(pageable);
    }
}
