package am.shopping.cart.service.impl;

import am.shopping.cart.bean.dto.request.CreateUserDto;
import am.shopping.cart.bean.dto.request.SignInDto;
import am.shopping.cart.bean.dto.response.AuthenticationResponseDto;
import am.shopping.cart.bean.dto.response.SignUpResponseDto;
import am.shopping.cart.bean.entity.Role;
import am.shopping.cart.bean.entity.ShoppingCart;
import am.shopping.cart.bean.entity.User;
import am.shopping.cart.exception.OperationFailedException;
import am.shopping.cart.mapper.UserMapper;
import am.shopping.cart.repository.RoleRepository;
import am.shopping.cart.repository.UserRepository;
import am.shopping.cart.security.jwt.JwtUtil;
import am.shopping.cart.service.ShoppingCartService;
import am.shopping.cart.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private AuthenticationManager authenticationManager;
    private JwtUtil jwtTokenUtil;
    private UserRepository userRepository;
    private UserMapper userMapper;
    private BCryptPasswordEncoder passwordEncoder;
    private RoleRepository roleRepository;
    private ShoppingCartService shoppingCartService;

    @Autowired
    public UserServiceImpl(AuthenticationManager authenticationManager, JwtUtil jwtTokenUtil, UserRepository userRepository, UserMapper userMapper, BCryptPasswordEncoder passwordEncoder, RoleRepository roleRepository, ShoppingCartService shoppingCartService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
        this.userRepository = userRepository;
        this.userMapper = userMapper;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
        this.shoppingCartService = shoppingCartService;
    }

    @Override
    public SignUpResponseDto addUser(CreateUserDto createUserDto) {
        User user = userMapper.map(createUserDto, User.class);
        user.setName(createUserDto.getName());
        user.setSurname(createUserDto.getSurname());
        user.setEmail(createUserDto.getEmail());
        user.setUsername(createUserDto.getUsername());
        user.setPassword(passwordEncoder.encode(createUserDto.getPassword()));
        Optional<Role> role = roleRepository.findByRole("USER");
        if (role.isPresent()) {
            List<Role> roleNameSet = Collections.singletonList(role.get());
            user.setRoles(roleNameSet);
        }
        user.setEnabled(true);
        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setCredentialsNonExpired(true);
        ShoppingCart shoppingCart = shoppingCartService.save(new ShoppingCart(user));
        user.setShoppingCart(shoppingCart);
        userRepository.save(user);
        SignUpResponseDto signUpResponseDto = userMapper.map(user, SignUpResponseDto.class);
        return signUpResponseDto;
    }

    @Override
    public Optional<User> findUserByUsername(String username) {
        return userRepository.findUserByUsername(username);
    }

    @Override
    public AuthenticationResponseDto signIn(SignInDto signInDto) {
        User user = userMapper.map(signInDto, User.class);
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword()));
        final String jwt = jwtTokenUtil.generateToken(authentication);
        return new AuthenticationResponseDto(jwt);
    }

    @Override
    public boolean isUsernameUnique(String username) {
        return !userRepository.findUserByUsername(username).isPresent();
    }

    @Override
    public User findUserById(Long id) {
        User user = userRepository.findById(id).orElseThrow(
                () -> new OperationFailedException(" User does not exist "));
        return user;
    }


}
