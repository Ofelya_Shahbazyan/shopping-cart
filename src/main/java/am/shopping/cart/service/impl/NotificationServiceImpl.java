package am.shopping.cart.service.impl;

import am.shopping.cart.bean.entity.Notification;
import am.shopping.cart.bean.entity.User;
import am.shopping.cart.mapper.NotificationMapper;
import am.shopping.cart.repository.NotificationRepository;
import am.shopping.cart.service.NotificationService;
import am.shopping.cart.util.CurrentUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class NotificationServiceImpl implements NotificationService {

    private NotificationRepository notificationRepository;
    private NotificationMapper notificationMapper;
    private CurrentUser currentUser;

    public NotificationServiceImpl(NotificationRepository notificationRepository, NotificationMapper notificationMapper,
                                   CurrentUser currentUser) {
        this.notificationRepository = notificationRepository;
        this.notificationMapper = notificationMapper;
        this.currentUser = currentUser;
    }

    @Override
    public void addNotification(Notification notification) {
        notificationRepository.save(notification);
    }

    @Override
    public Page<Notification> getNotificationByRecipient(Pageable pageable) {
        User user = currentUser.getCurrentUser();
        Page<Notification> notifications = notificationRepository.getNotificationByUser(user, pageable);
//        return (Page<NotificationDto>) notificationMapper.mapAsList(notifications, NotificationDto.class);
        return notifications;
    }

}

