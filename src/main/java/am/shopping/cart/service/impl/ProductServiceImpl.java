package am.shopping.cart.service.impl;

import am.shopping.cart.bean.dto.request.AddProductDto;
import am.shopping.cart.bean.dto.request.EditProductDto;
import am.shopping.cart.bean.dto.response.ProductResponseDto;
import am.shopping.cart.bean.entity.Product;
import am.shopping.cart.mapper.ProductMapper;
import am.shopping.cart.repository.ProductRepository;
import am.shopping.cart.service.ProductService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Optional;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository;
    private ProductMapper productMapper;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository, ProductMapper productMapper) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
    }

    @Override
    public ProductResponseDto addProduct(AddProductDto addProductDto) {
        Product product = productMapper.map(addProductDto, Product.class);
        Optional<Product> productInDb = productRepository.findProductById(addProductDto.getId());
        ProductResponseDto productResponseDto;
        if (productInDb.isPresent()) {
            productInDb.get().setCountInStock(productInDb.get().getCountInStock() + 1);
            productInDb.get().setUpdatedDate(new Date());
            productRepository.save(productInDb.get());
            productResponseDto = productMapper.map(productInDb.get(), ProductResponseDto.class);
        } else {
            product.setName(addProductDto.getName());
            product.setProductType(addProductDto.getProductType());
            product.setPrice(addProductDto.getPrice());
            product.setAddedDate(new Date());
            product.setCountInStock(1);
            product.setUpdatedDate(new Date());
            productRepository.save(product);
            productResponseDto = productMapper.map(product, ProductResponseDto.class);
        }
        return productResponseDto;
    }

    @Override
    public void deleteById(Long id) throws NotFoundException {
        Optional<Product> product = productRepository.findProductById(id);
        if (product.isPresent()) {
            productRepository.deleteById(id);
        } else {
            throw new NotFoundException("There is no product with this name!");
        }
    }

    @Override
    public Product editProduct(Long id, EditProductDto editProductDto) throws NotFoundException {
        Optional<Product> product = productRepository.findProductById(id);
        if (!product.isPresent()) {
            throw new NotFoundException("There is no product with this name!");
        } else {
            product.get().setName(editProductDto.getName());
            product.get().setPrice(editProductDto.getPrice());
            product.get().setProductType(editProductDto.getProductType());
            product.get().setUpdatedDate(new Date());
            productRepository.save(product.get());
        }
        return product.get();
    }
}

