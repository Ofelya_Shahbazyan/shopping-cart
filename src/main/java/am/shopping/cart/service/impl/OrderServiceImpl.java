package am.shopping.cart.service.impl;

import am.shopping.cart.bean.dto.request.CreateOrderDto;
import am.shopping.cart.bean.dto.response.OrderResponseDto;
import am.shopping.cart.bean.entity.Notification;
import am.shopping.cart.bean.entity.Order;
import am.shopping.cart.bean.entity.Product;
import am.shopping.cart.bean.entity.User;
import am.shopping.cart.exception.OperationFailedException;
import am.shopping.cart.mapper.OrderMapper;
import am.shopping.cart.repository.OrderRepository;
import am.shopping.cart.repository.ProductRepository;
import am.shopping.cart.repository.UserRepository;
import am.shopping.cart.service.NotificationService;
import am.shopping.cart.service.OrderService;
import am.shopping.cart.util.CurrentUser;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    private OrderMapper orderMapper;
    private ProductRepository productRepository;
    private CurrentUser currentUser;
    private OrderRepository orderRepository;
    private UserRepository userRepository;
    private NotificationService notificationService;

    @Autowired
    public OrderServiceImpl(OrderMapper orderMapper, ProductRepository productRepository, CurrentUser currentUser, OrderRepository orderRepository, UserRepository userRepository, NotificationService notificationService) {
        this.orderMapper = orderMapper;
        this.productRepository = productRepository;
        this.currentUser = currentUser;
        this.orderRepository = orderRepository;
        this.userRepository = userRepository;
        this.notificationService = notificationService;
    }

    @Override
    public OrderResponseDto newOrder(Long id, CreateOrderDto createOrderDto) throws NotFoundException {
        Order order = orderMapper.map(createOrderDto, Order.class);
        Optional<Product> productInDb = productRepository.findProductById(id);
        User user = currentUser.getCurrentUser();
        if (!productInDb.isPresent()) {
            throw new NotFoundException("There is no product with this name!");
        } else if (order.getTotalCount() > productInDb.get().getCountInStock()) {
            throw new OperationFailedException("There are not enough products in stock.");
        } else {
            order.setBuyerEmail(user.getEmail());
            order.setBuyerName(user.getName());
            order.setBuyerSurname(user.getSurname());
            order.setProductName(productInDb.get().getName());
            order.setProductPrice(productInDb.get().getPrice());
            order.setCountInStock(productInDb.get().getCountInStock());
            order.setTotalCount(order.getTotalCount());
            order.setProductType(productInDb.get().getProductType().getType());
            order.setOrderStatus("New");
            order.setTotalAmount(BigDecimal.valueOf(order.getTotalCount() * productInDb.get().getPrice().intValue()));
            order.setCreatedTime(new Date());
            order.setUpdatedTime(new Date());
            order.setShoppingCart(user.getShoppingCart());
            orderRepository.saveAndFlush(order);
            Notification notification = Notification.builder()
                    .admin(userRepository.findById((long) 1).get())
                    .user(currentUser.getCurrentUser())
                    .message("Well done! The order successfully created and has  'New' status.")
                    .build();
            notificationService.addNotification(notification);
        }
        OrderResponseDto orderResponseDto = orderMapper.map(order, OrderResponseDto.class);
        return orderResponseDto;
    }

    @Override
    public Page<Order> findAllOrders(User user, Pageable pageable) {
        Page<Order> orders = orderRepository.findAllByShoppingCart(user.getShoppingCart().getId(), pageable);
        return orders;
    }

}
