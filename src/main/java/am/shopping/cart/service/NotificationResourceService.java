package am.shopping.cart.service;

import am.shopping.cart.bean.entity.Notification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface NotificationResourceService {

    Page<Notification> getAllNotifications(Pageable pageable);
}
