package am.shopping.cart.service;

import am.shopping.cart.bean.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductResourceService {

    Page<Product> findAll(Pageable pageable);

    Product findByNameLike(String name);

    Page<Product> findByProductType_TypeLike(String type, Pageable pageable);

    Page<Product> findAllByOrderByPriceAsc(Pageable pageable);
}
