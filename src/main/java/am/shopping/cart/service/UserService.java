package am.shopping.cart.service;

import am.shopping.cart.bean.dto.request.CreateUserDto;
import am.shopping.cart.bean.dto.request.SignInDto;
import am.shopping.cart.bean.dto.response.AuthenticationResponseDto;
import am.shopping.cart.bean.dto.response.SignUpResponseDto;
import am.shopping.cart.bean.entity.User;

import java.util.Optional;

public interface UserService {

    AuthenticationResponseDto signIn(SignInDto signInDto);

    boolean isUsernameUnique(String value);

    SignUpResponseDto addUser(CreateUserDto createUserDto);

    Optional<User> findUserByUsername(String username);

    User findUserById(Long id);
}
