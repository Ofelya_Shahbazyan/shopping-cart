package am.shopping.cart.service;

import am.shopping.cart.bean.entity.ProductType;

import java.util.Optional;

public interface ProductTypeService {

    Optional<ProductType> findByTypeAndId(String type, Long id);
}
