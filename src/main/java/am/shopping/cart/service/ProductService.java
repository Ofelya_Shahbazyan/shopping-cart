package am.shopping.cart.service;

import am.shopping.cart.bean.dto.request.AddProductDto;
import am.shopping.cart.bean.dto.request.EditProductDto;
import am.shopping.cart.bean.dto.response.ProductResponseDto;
import am.shopping.cart.bean.entity.Product;
import javassist.NotFoundException;

public interface ProductService {

    ProductResponseDto addProduct(AddProductDto addProductDto);

    void deleteById(Long id) throws NotFoundException;

    Product editProduct(Long id, EditProductDto editProductDto) throws NotFoundException;
}
