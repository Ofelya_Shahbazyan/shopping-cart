package am.shopping.cart.service;

import am.shopping.cart.bean.entity.ShoppingCart;

public interface ShoppingCartService {

    ShoppingCart save(ShoppingCart shoppingCart);

}
