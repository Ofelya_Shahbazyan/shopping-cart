package am.shopping.cart.service;

import am.shopping.cart.bean.entity.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface OrderResourceService {

    Page<Order> findAllOrders(Pageable pageable);

    Order findById(Long id);

    void accept(Long id);

    void reject(Long id);
}
