package am.shopping.cart.service;

import am.shopping.cart.bean.dto.request.CreateOrderDto;
import am.shopping.cart.bean.dto.response.OrderResponseDto;
import am.shopping.cart.bean.entity.Order;
import am.shopping.cart.bean.entity.User;
import javassist.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface OrderService {

    OrderResponseDto newOrder(Long id, CreateOrderDto createOrderDto) throws NotFoundException;

    Page<Order> findAllOrders(User user, Pageable pageable);

}

