package am.shopping.cart.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = ProductTypeValidator.class)
@Documented
public @interface ProductTypeValidation {

    String message() default "Product type must not be empty. Allowed types: Book, Food, Drink, Clothe, Toy.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
