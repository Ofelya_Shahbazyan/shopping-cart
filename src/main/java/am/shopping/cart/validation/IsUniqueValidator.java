package am.shopping.cart.validation;

import am.shopping.cart.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

class IsUniqueValidator implements ConstraintValidator<IsUniqueValidation, String> {

    private String fieldName;
    private final UserService userService;

    @Autowired
    public IsUniqueValidator(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void initialize(IsUniqueValidation constraintAnnotation) {
        fieldName = constraintAnnotation.fieldName();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (fieldName.equals("Username")) {
            return userService.isUsernameUnique(value);
        }
        return false;
    }

}
