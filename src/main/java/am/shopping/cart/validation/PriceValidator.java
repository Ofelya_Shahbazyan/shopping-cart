package am.shopping.cart.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;

public class PriceValidator implements ConstraintValidator<PriceValidation, BigDecimal> {

    BigDecimal min = BigDecimal.valueOf(1);

    @Override
    public boolean isValid(BigDecimal value, ConstraintValidatorContext context) {
        return (value != null && value.compareTo(min) == 1);
    }
}
