package am.shopping.cart.validation;

import am.shopping.cart.bean.entity.ProductType;
import am.shopping.cart.service.ProductTypeService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ProductTypeValidator implements ConstraintValidator<ProductTypeValidation, ProductType> {

    //List<String> types = Arrays.asList("Book", "Food", "Drink", "Toy", "Clothe");
    private ProductTypeService productTypeService;

    @Autowired
    public ProductTypeValidator(ProductTypeService productTypeService) {
        this.productTypeService = productTypeService;
    }

    @Override
    public boolean isValid(ProductType productType, ConstraintValidatorContext constraintValidatorContext) {
        return productTypeService.findByTypeAndId(productType.getType(), productType.getId()).isPresent();
        //return (types.contains(productType.getType()) && productType != null);
    }
}
